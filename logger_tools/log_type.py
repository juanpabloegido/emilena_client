from enum import Enum


class LogType(Enum):
	DBG = "DEBUG"
	INF = "INFO"
	WRN = "WARNING"
	ERR = "ERROR"
