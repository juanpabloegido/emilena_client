def obfuscate_string(raw_value: str, proportion_visible: float = 0.2) -> str:
	if raw_value is None:
		return str(raw_value)

	total_len: int = len(raw_value)
	visible_len: int = min(total_len, max(int(total_len * proportion_visible), 0))
	invisible_len: int = total_len - visible_len

	return "*" * invisible_len + raw_value[-visible_len:] if visible_len > 0 else "*" * total_len
