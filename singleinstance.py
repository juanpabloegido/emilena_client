import logging
import socket
from typing import Optional
from logger import Logger


def check_socket(port: int) -> bool:
    try:
        single_instance_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        single_instance_socket.bind(('localhost', port))
        single_instance_socket.close()
        return True
    except Exception as e:
        Logger.error(f"Could not lock socket, assuming already locked: {e!r}")
        return False


def lock_socket(port: int) -> Optional[socket.socket]:
    try:
        single_instance_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        single_instance_socket.bind(('localhost', port))
        return single_instance_socket
    except Exception as e:
        Logger.error(f"Could not lock socket, assuming already locked: {e!r}")
        return None

if __name__ == '__main__':
    pass