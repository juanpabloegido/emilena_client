from typing import Dict, Callable, Optional

from context import AppContext
from .implementations import KeyringSecretReader
from .base import ISecretReader


class SecretReader:
	_reader: ISecretReader = KeyringSecretReader('guardian')

	@classmethod
	def read_secret(cls, name_of_secret: str) -> Optional[str]:
		return cls._reader.get_secret_value(name_of_secret)
