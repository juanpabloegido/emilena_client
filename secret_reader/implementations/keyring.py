from typing import Optional

from keyring.backend import KeyringBackend
from keyring.backends.Windows import WinVaultKeyring

from ..base import ISecretReader


class KeyringSecretReader(ISecretReader):
    _service: str
    _keyring: KeyringBackend

    def __init__(self, account_name: str):
        self._service = account_name
        self._keyring = WinVaultKeyring()

    def get_secret_value(self, name_of_secret: str) -> Optional[str]:
        return self._keyring.get_password(self._service, name_of_secret)
