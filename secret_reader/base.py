from abc import ABC, abstractmethod
from typing import Optional


class ISecretReader(ABC):
	@abstractmethod
	def get_secret_value(self, name_of_secret: str) -> Optional[str]:
		pass
