import logging
import sys
from db import Config, Database
import pymysql

from db import rds_config
from shared import *
from logger import Logger


def sync_users(conn: pymysql.connections):
    payload_obj = {"handler": "get_users"}
    response = invoke_lambda_function(function_name='emilena_sls', payload=payload_obj)
    res_json = json.loads(response['Payload'].read().decode("utf-8"))

    if res_json['statusCode'] == 200:
        result = res_json['body']

        with conn.cursor() as cur:

            query_exist = """SELECT COUNT(*) AS n FROM user WHERE username = %s"""

            sql_insert = "INSERT INTO user (created_on, updated_on, username, name, password, nif) VALUES (%s, %s, %s, %s, %s, %s)"

            sql_update = "UPDATE user SET created_on=%s, updated_on=%s, name=%s, password=%s, nif=%s WHERE username = %s"

            ##cur.execute('SELECT username FROM user')

            ##users = row = [item[0] for item in cur.fetchall()]
            n = 0
            inserts_number = 0
            updateds_number = 0
            for data in result:
                res = cur.execute(query_exist, data['email'])
                val = cur.fetchone()
                n = n + 1
                if val[0] == 1:
                    try:

                        cur.execute(sql_update, (
                            data['created_on'], data['updated_on'],
                            data['name'], data['password'], data['nif'], data['email']))
                        conn.commit()
                        updateds_number += 1
                        print(n, ' updated')

                    except Exception as e:
                        print('Error ', e)

                else:
                    try:
                        cur.execute(sql_insert, (
                            data['created_on'], data['updated_on'],
                            data['email'], data['name'], data['password'], data['nif']))
                        conn.commit()
                        inserts_number += 1
                        print(n, ' inserted')

                    except Exception as e:
                        print('Error ', e)

            print('insertados ', inserts_number)
            print('actualizados ', updateds_number)


def sync_clients(conn: pymysql.connections):
    with conn.cursor() as cur:
        cur.execute("SELECT * FROM client_master WHERE sync_date < updated_on OR sync_date is NULL")
    conn.commit()

    records = list()
    event = cur.fetchall()

    Cipher = AesCipher.from_secret()

    for row in event:
        record = {
            'id': row[0],
            'created_on': str(row[1]),
            'updated_on': date_to_str(datetime.datetime.now()),
            'nif': Cipher.decrypt_from_b64_decode(row[3]),
            'name': Cipher.decrypt_from_b64_decode(row[4]),
            'alias': Cipher.decrypt_from_b64_decode(row[5]),
            'gender': Cipher.decrypt_from_b64_decode(row[6]),
            'birth_date': Cipher.decrypt_from_b64_decode(row[7]),
            'email': Cipher.decrypt_from_b64_decode(row[8]),
            'phone': Cipher.decrypt_from_b64_decode(row[9]),
            'country': Cipher.decrypt_from_b64_decode(row[10]),
            'province': Cipher.decrypt_from_b64_decode(row[11]),
            'locality': Cipher.decrypt_from_b64_decode(row[12]),
            'postal_code': Cipher.decrypt_from_b64_decode(row[13]),
            'address': Cipher.decrypt_from_b64_decode(row[14]),
            'is_access_denied': row[15],
            'is_problematic': row[16],
            'is_vip': row[17],
            'is_professional_player': row[18],
            'observations': row[19],
            'incidence_report': row[20],
            'doc_is_not_nif': row[21],
            'doc_expiration_date': Cipher.decrypt_from_b64_decode(row[22])
        }
        records.append(record)

    payload_obj = {"handler": "push_clients", "body": records}

    response = invoke_lambda_function(function_name='emilena_sls', payload=payload_obj)
    res_json = json.loads(response['Payload'].read().decode("utf-8"))

    if res_json['statusCode'] == 200:
        print('Synced')


if __name__ == '__main__':
    db_host = rds_config.db_host
    db_username = rds_config.db_username
    db_password = rds_config.db_password
    db_name = rds_config.db_name
    db_port = rds_config.db_port

    try:
        conn = pymysql.connect(host=db_host, port=db_port, user=db_username, passwd=db_password, db=db_name,
                               connect_timeout=100)

        sync_clients(conn)
        sync_users(conn)

    except pymysql.MySQLError as e:
        Logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
        Logger.error(e)
