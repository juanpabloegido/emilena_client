from typing import TypeVar, Callable, Optional, Tuple

InputType: TypeVar = TypeVar('InputType')
OutputType: TypeVar = TypeVar('OutputType')


def to_safe_parser(
		unsafe_parser: Callable[[InputType], OutputType],
		default_value: Optional[OutputType] = None,
		allow_none: bool = False
) -> Callable[[Optional[InputType]], Optional[OutputType]]:
	def _safe_parser(raw_input: Optional[InputType]) -> Optional[OutputType]:
		if raw_input is None and not allow_none:
			return default_value

		# noinspection PyBroadException
		try:
			output_value: OutputType = unsafe_parser(raw_input)
			return output_value
		except Exception:
			return default_value

	return _safe_parser


def apply_parser_safely(
		raw_input: InputType,
		unsafe_parser: Callable[[InputType], OutputType],
		default_value: Optional[OutputType] = None,
		allow_none: bool = False
) -> Optional[OutputType]:
	return to_safe_parser(unsafe_parser, default_value, allow_none)(raw_input)


def fail_on_none(
		parser: Callable[[Optional[InputType]], Optional[OutputType]]
) -> Callable[[Optional[InputType]], Optional[OutputType]]:
	def _modified_parser(raw_input: Optional[InputType]) -> Optional[OutputType]:
		if raw_input is None:
			raise ValueError("{} not allowed".format(None))
		return parser(raw_input)

	return _modified_parser


def fail_on_empty(
		parser: Callable[[Optional[InputType]], Optional[OutputType]]
) -> Callable[[Optional[InputType]], Optional[OutputType]]:
	def _modified_parser(raw_input: Optional[InputType]) -> Optional[OutputType]:
		if raw_input is not None:
			try:
				length: int = len(raw_input)
			except Exception as e:
				raise ValueError("Could not get length of {}, not allowed to be empty: {!r}".format(raw_input, e))

			if length == 0:
				raise ValueError("{!r} not allowed to be empty".format(raw_input))
		return parser(raw_input)

	return _modified_parser


def none_bypass(
		parser: Callable[[InputType], OutputType]
) -> Callable[[Optional[InputType]], Optional[OutputType]]:
	def _modified_parser(raw_input: Optional[InputType]) -> Optional[OutputType]:
		if raw_input is None:
			return None
		return parser(raw_input)

	return _modified_parser


def empty_to_none(
		parser: Callable[[InputType], OutputType]
) -> Callable[[InputType], OutputType]:
	def _modified_parser(raw_input: InputType) -> Optional[OutputType]:
		if hasattr(raw_input, '__len__') and len(raw_input) == 0:
			return None
		return parser(raw_input)

	return _modified_parser


def none_to_val(
		parser: Callable[[InputType], OutputType],
		def_value: OutputType

) -> Callable[[Optional[InputType]], OutputType]:
	def _modified_parser(raw_value: Optional[InputType]) -> OutputType:
		if raw_value is None:
			return def_value
		return parser(raw_value)

	return _modified_parser


def accept_missing(
		parser: Callable[[Optional[InputType]], Optional[OutputType]]
) -> Callable[[bool, Optional[InputType]], Tuple[bool, Optional[OutputType]]]:
	def _modified_parser(_: bool, raw_value: InputType) -> Tuple[bool, Optional[OutputType]]:
		return True, parser(raw_value)

	return _modified_parser


def reject_missing(
		parser: Callable[[Optional[InputType]], Optional[OutputType]]
) -> Callable[[bool, Optional[InputType]], Tuple[bool, Optional[OutputType]]]:
	def _modified_parser(is_present: bool, raw_value: InputType) -> Tuple[bool, Optional[OutputType]]:
		if not is_present:
			return False, None
		return True, parser(raw_value)

	return _modified_parser
