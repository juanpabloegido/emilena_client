from enum import Enum, auto
from typing import TypeVar, Callable, Optional, Generic, Tuple, Any, Union, Dict, Type, List

InputPropertyType: TypeVar = TypeVar('InputPropertyType')
ParsedPropertyType: TypeVar = TypeVar('ParsedPropertyType')
OutputPropertyType: TypeVar = TypeVar('OutputPropertyType')

InputParser: TypeVar = Callable[[bool, Optional[InputPropertyType]], Tuple[bool, ParsedPropertyType]]
OutputParser: TypeVar = Callable[[bool, Optional[ParsedPropertyType]], Tuple[bool, OutputPropertyType]]

ConstructedType: TypeVar = TypeVar('ConstructedType', bound=object)


class JsonPropertyPolicy(Enum):
	REQUIRED = auto()
	REJECTED_ALLOWED = auto()
	OMIT = auto()

	@classmethod
	def default_policy(cls) -> 'JsonPropertyPolicy':
		return cls.REQUIRED

	@classmethod
	def default_policy_tuple(cls) -> Tuple['JsonPropertyPolicy', 'JsonPropertyPolicy']:
		return cls.default_policy(), cls.default_policy()


class JsonProperty(Generic[InputPropertyType, ParsedPropertyType, OutputPropertyType]):
	_field_name: str
	_input_parser: InputParser
	_output_parser: OutputParser
	_parsing_policy: Tuple[JsonPropertyPolicy, JsonPropertyPolicy]

	def __init__(
			self,
			field_name: str,
			input_parser: InputParser = None,
			output_parser: OutputParser = None,
			parsing_policy: Tuple[JsonPropertyPolicy, JsonPropertyPolicy] = ...
	):
		self._field_name = field_name
		self._set_input_parser(input_parser)
		self._set_output_parser(output_parser)
		self._output_parser = type(self)._reject_on_output_exception(output_parser) \
			if output_parser is not None else None
		self._parsing_policy = parsing_policy
		if self._parsing_policy is ...:
			self._parsing_policy = JsonPropertyPolicy.default_policy_tuple()

	def _set_input_parser(self, input_parser: InputParser) -> None:
		self._input_parser = \
			type(self)._reject_on_input_exception(input_parser) if input_parser is not None else None

	def _set_output_parser(self, output_parser: OutputParser) -> None:
		self._output_parser = \
			type(self)._reject_on_output_exception(output_parser) if output_parser is not None else None

	@staticmethod
	def _reject_on_input_exception(parser: InputParser) -> InputParser:
		def _modified_parser(
				is_present: bool, raw_value: Optional[InputPropertyType]
		) -> Tuple[bool, Optional[OutputPropertyType]]:
			# noinspection PyBroadException
			try:
				return parser(is_present, raw_value)
			except Exception:
				return False, None

		return _modified_parser

	@staticmethod
	def _reject_on_output_exception(parser: OutputParser) -> OutputParser:
		def _modified_parser(
				is_present: bool, raw_value: Optional[ParsedPropertyType]
		) -> Tuple[bool, Optional[OutputPropertyType]]:
			# noinspection PyBroadException
			try:
				return parser(is_present, raw_value)
			except Exception:
				return False, None

		return _modified_parser

	def parse_input(self, parsed_json: Dict[str, Any]) -> Optional[ParsedPropertyType]:
		try:
			value_present: bool = self._field_name in parsed_json
			raw_value: Optional[InputPropertyType] = parsed_json.get(self._field_name)

			if self._input_parser is not None:
				accepted, parsed_value = self._input_parser(value_present, raw_value)
			else:
				accepted, parsed_value = value_present, raw_value

		except Exception as e:
			raise JsonInputParsingException(
				"Exception raised parsing field ({}) from ({}): {!r}".format(
					self._field_name, parsed_json, repr(e)))

		if not accepted and self._parsing_policy[0] == JsonPropertyPolicy.REQUIRED:
			raise JsonInputParsingException('Required field {} not accepted'.format(self._field_name))

		return parsed_value

	def parse_output(self, target: object, attr_name: str) -> Optional[OutputPropertyType]:
		try:
			value_present: bool = hasattr(target, attr_name)
			raw_value: Optional[ParsedPropertyType] = getattr(target, attr_name, None)

			if self._output_parser is not None:
				accepted, parsed_value = self._output_parser(value_present, raw_value)
			else:
				accepted, parsed_value = value_present, raw_value

		except Exception as e:
			raise JsonInputParsingException(repr(e))

		if not accepted and self._parsing_policy[0] == JsonPropertyPolicy.REQUIRED:
			raise JsonOutputParsingException('Required field {} not accepted'.format(attr_name))

		return parsed_value

	def get_field_name(self) -> str:
		return self._field_name

	def omit_on_input(self) -> bool:
		return self._parsing_policy[0] == JsonPropertyPolicy.OMIT

	def omit_on_output(self) -> bool:
		return self._parsing_policy[1] == JsonPropertyPolicy.OMIT


JsonPropertyType: TypeVar = \
	Union[JsonProperty[InputPropertyType, ParsedPropertyType, OutputPropertyType], Optional[ParsedPropertyType]]


def construct_object(target_type: Type[ConstructedType], parsed_json: Dict[str, Any]) -> ConstructedType:
	args: Dict[str, Any] = {}

	for target_type_key, target_type_value in target_type.__dict__.items():
		if issubclass(type(target_type_value), JsonProperty):
			target_type_value: JsonProperty = target_type_value
			if target_type_value.omit_on_input():
				continue
			parsed_value: Any = target_type_value.parse_input(parsed_json)
			args[target_type_key] = parsed_value

	return target_type(**args)


def construct_objects(
		target_type: Type[ConstructedType], parsed_json_list: List[Dict[str, Any]]
) -> List[ConstructedType]:
	return list(map(
		lambda parsed_json: construct_object(target_type, parsed_json),
		parsed_json_list
	))


def generate_json(target: ConstructedType) -> Dict[str, Any]:
	rv: Dict[str, Any] = {}
	target_type: Type[ConstructedType] = type(target)

	for target_type_key, target_type_value in target_type.__dict__.items():
		if issubclass(type(target_type_value), JsonProperty):
			target_type_value: JsonProperty = target_type_value
			if target_type_value.omit_on_output():
				continue
			parsed_value: Any = target_type_value.parse_output(target, target_type_key)
			rv[target_type_value.get_field_name()] = parsed_value

	return rv


def generate_jsons(targets: ConstructedType) -> List[Dict[str, Any]]:
	return list(map(
		generate_json,
		targets
	))


class JsonInputParsingException(Exception):
	pass


class JsonOutputParsingException(Exception):
	pass
