import datetime
import functools
from typing import Dict, Type, Callable, TypeVar, List, Any

from .json_parsing import JsonProperty, JsonPropertyPolicy, generate_jsons, construct_object, generate_json, \
	construct_objects
from .json_properties import ITwoWayJsonProperty, TwoWayParsedPropertyType, TwoWayRawPropertyType
from .meta_parsers import none_bypass, empty_to_none, none_to_val


class TwoWayJsonPropertyDictToObject(ITwoWayJsonProperty[Dict[str, Any], TwoWayParsedPropertyType]):
	_constructed_type: Type[TwoWayParsedPropertyType]

	def __init__(
			self,
			field_name: str, constructed_type: Type[TwoWayParsedPropertyType],
			accepts_missing: bool = True, allows_none: bool = True
	):
		self._constructed_type = constructed_type
		super().__init__(field_name, accepts_missing, allows_none, True)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(functools.partial(construct_object, self._constructed_type))

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(generate_json)


PropertyListItemType: TypeVar = TypeVar('PropertyListItemType')


class TwoWayJsonPropertyListPropertyObjects(ITwoWayJsonProperty[List[Dict[str, Any]], List[PropertyListItemType]]):
	_item_type: Type[PropertyListItemType]

	def __init__(
			self, field_name: str, item_type: Type[PropertyListItemType],
			accepts_missing: bool = True, allows_none: bool = True, allows_empty: bool = True
	):
		self._item_type = item_type
		super().__init__(field_name, accepts_missing, allows_none, allows_empty)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(functools.partial(construct_objects, self._item_type))

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(generate_jsons)


class OneWayJsonPropertyAny(JsonProperty[Any, Any, Any]):
	def __init__(self, field_name: str, accepts_missing: bool = True):
		policy: JsonPropertyPolicy = JsonPropertyPolicy.REJECTED_ALLOWED if accepts_missing \
			else JsonPropertyPolicy.REQUIRED
		super().__init__(field_name, parsing_policy=(policy, policy))


class OneWayJsonPropertyStr(ITwoWayJsonProperty[str, str]):
	_empty_is_none: bool

	def __init__(
			self,
			field_name: str, accepts_missing: bool = True, allows_none: bool = True, allows_empty: bool = True,
			empty_is_none: bool = False
	):
		self._empty_is_none = empty_is_none
		super().__init__(field_name, accepts_missing, allows_none, allows_empty)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		parser: Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType] = none_bypass(str)

		if self._empty_is_none:
			parser = empty_to_none(parser)

		return parser

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		parser: Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType] = none_bypass(str)

		if self._empty_is_none:
			parser = none_to_val(parser, "")

		return parser


class OneWayJsonPropertyInt(ITwoWayJsonProperty[int, int]):
	def __init__(
			self,
			field_name: str, accepts_missing: bool = True, allows_none: bool = True):
		super().__init__(field_name, accepts_missing, allows_none, True)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(lambda x: int(x))

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(lambda x: int(x))


class OneWayJsonPropertyFloat(ITwoWayJsonProperty[float, float]):
	def __init__(
			self,
			field_name: str, accepts_missing: bool = True, allows_none: bool = True):
		super().__init__(field_name, accepts_missing, allows_none, True)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(lambda x: float(x))

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(lambda x: float(x))


class OneWayJsonPropertyBool(ITwoWayJsonProperty[bool, bool]):

	def __init__(self, field_name: str, accepts_missing: bool = True, allows_none: bool = True):
		super().__init__(field_name, accepts_missing, allows_none, allows_empty=True)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(bool)

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(bool)


class TwoWayJsonPropertyIntToBool(ITwoWayJsonProperty[int, bool]):
	def __init__(
			self,
			field_name: str, accepts_missing: bool = True, allows_none: bool = True):
		super().__init__(field_name, accepts_missing, allows_none, True)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(bool)

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(int)


class TwoWayJsonPropertyStrToInt(ITwoWayJsonProperty[str, int]):
	def __init__(
			self,
			field_name: str, accepts_missing: bool = True, allows_none: bool = True):
		super().__init__(field_name, accepts_missing, allows_none, True)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(int)

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(str)


class TwoWayJsonPropertyStrToDatetime(ITwoWayJsonProperty[str, datetime.datetime]):
	_datetime_format: str

	def __init__(
			self,
			field_name: str, datetime_format: str, accepts_missing: bool = True, allows_none: bool = True):
		self._datetime_format = datetime_format
		super().__init__(field_name, accepts_missing, allows_none, True)

	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		return none_bypass(lambda x: datetime.datetime.strptime(x, self._datetime_format))

	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		return none_bypass(lambda x: x.strftime(self._datetime_format))
