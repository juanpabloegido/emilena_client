from abc import ABC, abstractmethod
from typing import Callable, TypeVar, Optional, Tuple

from .json_parsing import InputParser, JsonProperty, OutputParser, JsonPropertyType
from .meta_parsers import reject_missing, accept_missing, fail_on_empty, fail_on_none

BaseParserInputType: TypeVar = TypeVar('BaseParserInputType')
BaseParserOutputType: TypeVar = TypeVar('BaseParserOutputType')


def parser_constructor(
		base_parser: Callable[[BaseParserInputType], BaseParserOutputType],
		accepts_missing: bool = False, allows_none: bool = False, allows_empty: bool = False
) -> Callable[[bool, Optional[BaseParserInputType]], Tuple[bool, BaseParserOutputType]]:
	adapter: Callable[[InputParser], InputParser] = accept_missing if accepts_missing else reject_missing
	if not allows_empty:
		base_parser = fail_on_empty(base_parser)
	if not allows_none:
		base_parser = fail_on_none(base_parser)

	return adapter(base_parser)


TwoWayRawPropertyType: TypeVar = TypeVar('TwoWayRawPropertyType')
TwoWayParsedPropertyType: TypeVar = TypeVar('TwoWayParsedPropertyType')

TwoWayJsonPropertyType: TypeVar = \
	JsonPropertyType[TwoWayRawPropertyType, TwoWayParsedPropertyType, TwoWayRawPropertyType]
OneWayJsonPropertyType: TypeVar = TwoWayJsonPropertyType[TwoWayParsedPropertyType, TwoWayParsedPropertyType]


class ITwoWayJsonProperty(ABC, JsonProperty[TwoWayRawPropertyType, TwoWayParsedPropertyType, TwoWayRawPropertyType]):
	_accepts_missing: bool
	_allows_none: bool
	_allows_empty: bool

	def __init__(
			self, field_name: str, accepts_missing: bool, allows_none: bool, allows_empty: bool
	):
		self._accepts_missing = accepts_missing
		self._allows_none = allows_none
		self._allows_empty = allows_empty
		super().__init__(field_name)
		self._set_input_parser(self._get_input_parser())
		self._set_output_parser(self._get_output_parser())

	def _get_input_parser(self) -> InputParser:
		return parser_constructor(
			self._get_forwards_base_parser(), self._accepts_missing, self._allows_none, self._allows_empty)

	def _get_output_parser(self) -> OutputParser:
		return parser_constructor(
			self._get_backwards_base_parser(), self._accepts_missing, self._allows_none, self._allows_empty)

	@abstractmethod
	def _get_forwards_base_parser(self) -> Callable[[TwoWayRawPropertyType], TwoWayParsedPropertyType]:
		pass

	@abstractmethod
	def _get_backwards_base_parser(self) -> Callable[[TwoWayParsedPropertyType], TwoWayRawPropertyType]:
		pass
