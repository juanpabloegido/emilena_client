import datetime
from dataclasses import dataclass
from threading import RLock
from typing import Optional, Dict, Any, List, Tuple
from urllib.parse import urljoin

import jwt as jwt
import requests
from requests_aws4auth import AWS4Auth

from api.responses import RawConnection
from config import ConfigServerless, ConfigAws, Configuration, ConfigHall
from logger import Logger
from parsing import construct_object


class Token:
	_EXPIRY_DATETIME_MARGIN: datetime.timedelta = datetime.timedelta(
		minutes=5)  # A token will be considered expired if it's this close to do so

	_token: Optional[str]
	_expiry_datetime: Optional[datetime.datetime]
	_mutex: RLock

	def __init__(self):
		self._token = None
		self._expiry_datetime = None
		self._mutex = RLock()

	@classmethod
	def _extract_expiry_datetime(cls, token: str) -> datetime.datetime:
		payload: Dict[str, Any] = jwt.decode(token, verify=False)
		expiry_timestamp: int = payload['exp']
		return datetime.datetime.utcfromtimestamp(expiry_timestamp)

	def set_token(self, token: str) -> None:
		self._token = token
		self._expiry_datetime = type(self)._extract_expiry_datetime(token)

	def _is_expired(self) -> bool:
		return datetime.datetime.utcnow() + type(self)._EXPIRY_DATETIME_MARGIN > self._expiry_datetime

	def is_valid(self) -> bool:
		return self._token is not None and self._expiry_datetime is not None and not self._is_expired()

	def get_token(self) -> Optional[str]:
		if self.is_valid():
			return self._token
		return None

	def get_mutex(self) -> RLock:
		return self._mutex


@dataclass
class ApiUserCredentials:
	username: str
	password: str


@dataclass
class ApiLoginResponse:
	token: str
	user_email: str


class ServerlessApi:
	_AWS_SERVICE: str = 'execute-api'
	_G_AUTH_HEADER_FIELD: str = 'G-Auth'
	_G_AUTH_HEADER_VALUE_TEMPLATE: str = 'Bearer {}'

	_user_credentials: Optional[ApiUserCredentials]
	_token: Token

	_aws_key_id: str
	_aws_key_secret: str
	_aws_region: str
	_config: ConfigServerless
	_hall_code: str

	def __init__(
			self,
			user_credentials: Optional[ApiUserCredentials],
			aws_key_id: str, aws_key_secret: str, aws_region, config: ConfigServerless, hall_code: str,
			token: Optional[Token] = None
	):
		self._user_credentials = user_credentials
		self._token = token if token is not None else Token()

		self._aws_key_id = aws_key_id
		self._aws_key_secret = aws_key_secret
		self._aws_region = aws_region
		self._config = config
		self._hall_code = hall_code

	@classmethod
	def from_config(
			cls,
			user_credentials: Optional[ApiUserCredentials], token: Optional[Token] = None
	) -> 'ServerlessApi':
		aws_config: ConfigAws = Configuration.get_aws_config()
		hall_config: ConfigHall = Configuration.get_hall_config()
		return cls(
			user_credentials,
			aws_config.access_key, aws_config.secret_key, aws_config.region, aws_config.serverless,
			hall_config.hall_code,
			token
		)

	@classmethod
	def _from_instance(cls, instance: 'ServerlessApi') -> 'ServerlessApi':
		return cls(
			instance._user_credentials,
			instance._aws_key_id, instance._aws_key_secret, instance._aws_region, instance._config, instance._hall_code,
			instance._token
		)

	def clone(self) -> 'ServerlessApi':
		return type(self)._from_instance(self)

	def _get_aws_auth(self) -> AWS4Auth:
		auth: AWS4Auth = AWS4Auth(
			self._aws_key_id,
			self._aws_key_secret,
			self._aws_region,
			type(self)._AWS_SERVICE
		)

		return auth

	def _login(self) -> Tuple[Optional[bool], Optional[ApiLoginResponse]]:
		try:
			if self._user_credentials is None:
				return None, None

			payload: Dict[str, Any] = {
				'user_email': self._user_credentials.username,
				'password': self._user_credentials.password
			}

			complete_url: str = urljoin(self._config.base_url, self._config.endpoint_auth)
			r: requests.Response = requests.post(url=complete_url, json=payload, auth=self._get_aws_auth())
			response_body: Dict[str, Any] = r.json()
			if (
					r.status_code == 401 and response_body['code'] == 'INVALID_CREDENTIALS'  # TODO: copy over code list
			):  # Check for wrong credentials
				return False, None
			r.raise_for_status()

			login_response: ApiLoginResponse = ApiLoginResponse(
				token=response_body['response']['token'],
				user_email=response_body['response']['user_email']
			)

			return True, login_response
		except Exception as e:
			Logger.warn(f"Could not auth: {e!r}")
			return None, None

	def _generate_token(self) -> Optional[str]:
		login_ok, login_results = self._login()
		return login_results.token if login_ok is not None else None

	def _get_token(self) -> str:
		with self._token.get_mutex():
			saved_token: Optional[str] = self._token.get_token()
			if saved_token is not None:
				return saved_token
			else:
				generated_token: Optional[str] = self._generate_token()
				if generated_token is None:
					raise ValueError("No valid token, could not obtain new token")
				self._token.set_token(generated_token)
				return generated_token

	def _get_token_headers(self) -> Dict[str, str]:
		return {
			type(self)._G_AUTH_HEADER_FIELD: type(self)._G_AUTH_HEADER_VALUE_TEMPLATE.format(self._get_token())
		}

	def remove_credentials(self) -> None:
		self._user_credentials = None

	def add_credentials(self, user_credentials: ApiUserCredentials) -> None:
		self._user_credentials = user_credentials

	def verify_credentials(self) -> Tuple[Optional[bool], Optional[ApiLoginResponse]]:
		login_ok, login_response = self._login()
		if login_ok:
			self._token.set_token(login_response.token)
		return login_ok, login_response

	def has_credentials(self) -> bool:
		return self._user_credentials is not None

	def get_users(self) -> Optional[List[str]]:
		try:
			complete_url: str = urljoin(self._config.base_url, self._config.endpoint_get_users)
			r: requests.Response = requests.get(
				url=complete_url, auth=self._get_aws_auth(), headers=self._get_token_headers()
			)
			r.raise_for_status()

			response_body: Dict[str, Any] = r.json()
			return response_body['response']['users']

		except Exception as e:
			Logger.warn(f"Could not get ticket types {e!r}")

	def post_clients(self) -> Optional[List[str]]:
		try:
			complete_url: str = urljoin(self._config.base_url, self._config.endpoint_post_clients)
			r: requests.Response = requests.get(
				url=complete_url, auth=self._get_aws_auth(), headers=self._get_token_headers()
			)
			r.raise_for_status()

			response_body: Dict[str, Any] = r.json()
			return response_body['response']['users']

		except Exception as e:
			Logger.warn(f"Could not get ticket types {e!r}")

	def get_ticket_types(self) -> Optional[List[str]]:
		try:
			complete_url: str = urljoin(self._config.base_url, self._config.endpoint_get_ticket_types)
			r: requests.Response = requests.get(
				url=complete_url, auth=self._get_aws_auth(), headers=self._get_token_headers()
			)
			r.raise_for_status()

			response_body: Dict[str, Any] = r.json()
			return response_body['response']['ticket_types']
		except Exception as e:
			Logger.warn(f"Could not get ticket types {e!r}")

	def get_connections(self) -> Optional[List[RawConnection]]:
		try:
			complete_url: str = urljoin(self._config.base_url, self._config.endpoint_get_connections)
			query_parameters: Dict[str, str] = {
				'hall_code': self._hall_code
			}
			r: requests.Response = requests.get(
				url=complete_url, auth=self._get_aws_auth(), headers=self._get_token_headers(), params=query_parameters
			)
			r.raise_for_status()

			response_body: Dict[str, Any] = r.json()
			connections: List[Dict[str, Any]] = response_body['response']['connections']
			if len(connections) == 0:
				Logger.warn("No connections obtained")
				return []

			rv: List[RawConnection] = []
			for c in connections:
				try:
					parsed_connection: RawConnection = construct_object(RawConnection, c)
					rv.append(parsed_connection)
				except Exception as e:
					Logger.warn(f"Could not parse connection, skipping...: {e!r}")
			return rv

		except Exception as e:
			Logger.warn(f"Could not get connection data: {e!r}")
			return None
