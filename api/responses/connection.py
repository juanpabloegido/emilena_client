from dataclasses import dataclass
from typing import Dict, Any

from parsing import OneWayJsonPropertyType, OneWayJsonPropertyStr, OneWayJsonPropertyAny


@dataclass
class RawConnection:
	hall_code: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'hall_code', accepts_missing=False, allows_none=False
	)
	device: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'device', accepts_missing=False, allows_none=False
	)
	provider: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'provider', accepts_missing=False, allows_none=False
	)
	data: OneWayJsonPropertyType[Dict[str, Any]] = OneWayJsonPropertyAny(
		'data', accepts_missing=False)
