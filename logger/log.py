import datetime
import inspect
import os
import sys
from inspect import FrameInfo
from threading import Lock, RLock
from typing import Dict, IO, List, Sequence

from config import Configuration
from config.config_application import ConfigApplication, LogTypeConfig
from logger_tools.log_type import LogType


class Logger:
	print_mutex: Lock = Lock()
	id_mutex: Lock = Lock()
	file_mutex: RLock = RLock()
	message_id_count: int = 0

	open_files: Dict[str, IO] = {}
	file_output: Dict[LogType, List[IO]] = {}
	ready: bool = False

	@classmethod
	def _open_file(cls, filename: str) -> IO:
		with cls.file_mutex:
			open_file: IO = cls.open_files.get(filename)

			if open_file is None:
				try:
					open_file = open(
						os.path.join(Configuration.get_log_dir_path(), filename), mode='a+', buffering=1, encoding='utf-8')
					cls.open_files[filename] = open_file
				except Exception as e:
					print("Could not open file {}: {!r}".format(filename, e))
			return open_file

	@classmethod
	def _close_open_files(cls) -> None:
		with cls.file_mutex:
			filenames: List[str] = list(cls.open_files.keys())

			for filename in filenames:
				try:
					cls.open_files[filename].close()
					del cls.open_files[filename]
				except Exception as e:
					print("Could not close file {}: {!r}".format(filename, e))

	@classmethod
	def _create_files_for(
			cls, timestamp: datetime.date,
			log_type_config: LogTypeConfig) -> List[IO]:
		with cls.file_mutex:
			log_dir: str = Configuration.get_log_dir_path()
			os.makedirs(log_dir, exist_ok=True)

			rv: List[IO] = log_type_config.mirrors[:]

			for filename_suffix in log_type_config.filenames:
				filename = "{}{}".format(timestamp, filename_suffix)
				fd = cls._open_file(filename)
				if fd:
					rv.append(fd)

			return rv

	@classmethod
	def _populate_file_output(cls, app_config: ConfigApplication) -> Dict[LogType, List[IO]]:
		with cls.file_mutex:
			today: datetime.date = datetime.date.today()
			mapped_type_config: Dict[LogType, LogTypeConfig] = app_config.log_type_config
			return {
				log_type: cls._create_files_for(today, mapped_type_config[log_type]) for log_type in LogType
			}

	@classmethod
	def start_logging(cls) -> None:
		app_config: ConfigApplication = Configuration.get_app_config()
		cls.file_output = cls._populate_file_output(app_config)
		cls.ready = True

	@classmethod
	def end_logging(cls) -> None:
		cls.file_output = {}
		cls._close_open_files()
		cls.ready = False

	@staticmethod
	def _get_caller_frame() -> FrameInfo:
		return inspect.stack()[2]

	@classmethod
	def _get_message_id(cls) -> int:
		with cls.id_mutex:
			rv = cls.message_id_count
			cls.message_id_count += 1
			return rv

	@classmethod
	def _create_string(cls, msg, log_type: LogType, fi: FrameInfo) -> str:
		return "[{}]\t[{}: {}():{}]\t[{}]\t[{}]\t{}".format(
			log_type.value,
			fi.filename, fi.function, fi.lineno,
			datetime.datetime.now(),
			cls._get_message_id(),
			msg
		)

	@classmethod
	def _get_files_for(cls, log_type: LogType) -> [IO]:
		if cls.ready:
			return cls.file_output.get(log_type, [])
		return {
			LogType.DBG: [sys.stdout],
			LogType.INF: [sys.stdout],
			LogType.WRN: [sys.stderr],
			LogType.ERR: [sys.stderr]
		}.get(log_type, [])

	# TODO: multi-threaded logging
	@classmethod
	def log(cls, msg: str, log_type: LogType, files: Sequence[IO] = ..., fi: FrameInfo = ...) -> None:
		with cls.file_mutex:

			if files is ...:
				files = cls._get_files_for(log_type)

			if files is None:
				return

			if fi is ...:
				fi = cls._get_caller_frame()

			full_msg: str = cls._create_string(msg, log_type, fi)
			for file in files:
				try:
					cls.print_mutex.acquire()
					print(full_msg, file=file)
				finally:
					cls.print_mutex.release()

	@classmethod
	def debug(cls, msg: str, files: Sequence[IO] = ...) -> None:
		cls.log(msg, LogType.DBG, files, cls._get_caller_frame())

	@classmethod
	def info(cls, msg: str, files: Sequence[IO] = ...) -> None:
		cls.log(msg, LogType.INF, files, cls._get_caller_frame())

	@classmethod
	def warn(cls, msg: str, files: Sequence[IO] = ...) -> None:
		cls.log(msg, LogType.WRN, files, cls._get_caller_frame())

	@classmethod
	def error(cls, msg: str, files: Sequence[IO] = ...) -> None:
		cls.log(msg, LogType.ERR, files, cls._get_caller_frame())
