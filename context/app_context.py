class AppContext:
	APP_NAME: str = 'emilena'
	APP_VERSION: str = '1.0'

	@classmethod
	def get_base_name(cls) -> str:
		return cls.APP_NAME

	@classmethod
	def get_filename_identifier(cls) -> str:
		return '{}_{}'.format(cls.APP_NAME, cls.APP_VERSION)

	@classmethod
	def get_readable_name(cls) -> str:
		return "{} (versión {})".format(cls.APP_NAME.capitalize(), cls.APP_VERSION)
