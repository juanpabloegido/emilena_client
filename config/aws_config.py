from dataclasses import dataclass
from typing import Dict, Any, Union

from logger_tools import obfuscate_string
from parsing import OneWayJsonPropertyStr, JsonProperty, construct_object, TwoWayJsonPropertyDictToObject, \
	TwoWayJsonPropertyType
from .config_tools import IConfig
from .serverless_config import ConfigServerless


@dataclass
class ConfigAws(IConfig):
	access_key: Union[str, JsonProperty] = OneWayJsonPropertyStr('access_key')
	secret_key: Union[str, JsonProperty] = OneWayJsonPropertyStr('secret_key')
	region: Union[str, JsonProperty] = OneWayJsonPropertyStr('region')
	serverless: TwoWayJsonPropertyType[dict, ConfigServerless] = TwoWayJsonPropertyDictToObject(
		'serverless', ConfigServerless, accepts_missing=False, allows_none=False
	)

	@classmethod
	def from_json(cls, parsed_json: Dict[str, Any]) -> 'IConfig':
		return construct_object(cls, parsed_json)

	def __repr__(self) -> str:
		return \
			f"<{type(self)}" \
			f" access_key: ({obfuscate_string(self.access_key)})" \
			f" secret_key: ({obfuscate_string(self.secret_key)})" \
			f" region: ({self.region})" \
			f" serverless: ({self.serverless})" \
			">"
