from dataclasses import dataclass

from parsing import OneWayJsonPropertyType, OneWayJsonPropertyStr


@dataclass
class ConfigServerless:
	base_url: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'base_url',
		accepts_missing=False, allows_none=False
	)
	endpoint_auth: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'endpoint_auth',
		accepts_missing=False, allows_none=False
	)
	endpoint_get_ticket_types: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'endpoint_get_ticket_types',
		accepts_missing=False, allows_none=False
	)
	endpoint_get_connections: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'endpoint_get_connections',
		accepts_missing=False, allows_none=False
	)
	endpoint_get_users: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'endpoint_get_users',
		accepts_missing=False, allows_none=False
	)
	endpoint_post_clients: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'endpoint_post_clients',
		accepts_missing=False, allows_none=False
	)
