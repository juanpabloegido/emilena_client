import json
import sys
from abc import ABC, abstractmethod
from typing import Dict, Any, Callable


def try_to_parse(
		d: Dict[str, Any], field: str, parser: Callable[[Any], Any] = ...,
		default_generator: Callable[[], Any] = ...) -> Any:
	if parser is ...:
		def parser(raw_value: Any) -> Any:
			return raw_value

	if default_generator is ...:
		def default_generator() -> Any:
			return None

	try:
		return parser(d[field])
	except Exception as e:
		try:
			default_value: Any = default_generator()
		except Exception as e_:
			default_value = None
			print("Could not evaluate default value from {}, using {}: {!r}".format(
				default_generator, default_value, e_)
			)

		print(
			"Could not parse field {} from {}, using default {}: {!r}".format(field, d, default_value, e),
			file=sys.stderr
		)
		return default_value


class IConfig(ABC):
	@classmethod
	@abstractmethod
	def from_json(cls, parsed_json: Dict[str, Any]) -> 'IConfig':
		pass

	@classmethod
	def from_filename(cls, file_path: str) -> 'IConfig':
		with open(file_path) as open_file:
			parsed_json: Dict[str, Any] = json.load(open_file)
			return cls.from_json(parsed_json)
