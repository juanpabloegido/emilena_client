from dataclasses import dataclass
from typing import Dict, Any

from parsing import OneWayJsonPropertyType, OneWayJsonPropertyStr, construct_object
from .config_tools import IConfig


@dataclass
class ConfigHall(IConfig):
	hall_code: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'hall_code', accepts_missing=False, allows_none=False
	)

	@classmethod
	def from_json(cls, parsed_json: Dict[str, Any]) -> 'IConfig':
		return construct_object(cls, parsed_json)
