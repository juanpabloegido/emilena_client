import os
from typing import Optional

from .aws_config import ConfigAws
from .config_application import ConfigApplication
from .hall_config import ConfigHall
from .printer_config import ConfigPrinter


# TODO: move config files to JsonProperty parsing framework


class Configuration:
	_BASE_PATH: str = '.'
	_PARENT_PATH: str = '..'
	_CONFIG_SUBDIR: str = '.config'
	_LOG_SUBDIR: str = '.logs'
	_CACHE_SUBDIR: str = '.cache'
	_DATA_SUBDIR: str = '.data'
	_TEMP_SUBDIR: str = '.temp'

	_TICKET_TEMPLATE_FILENAME: str = 'ticket_template.jpg'
	_CAPABILITIES_FILENAME: str = 'capabilities.json'
	_FONT_FILENAME: str = 'font.ttf'

	_AWS_CONFIG_FILENAME: str = 'aws_config.json'
	_APP_CONFIG_FILENAME: str = 'app_config.json'
	_HALL_CONFIG_FILENAME: str = 'hall_config.json'
	_PRINTER_CONFIG_FILENAME: str = 'printer_config.json'

	_CACHE_FILENAME: str = 'cache.json'

	_cached_aws_config: Optional[ConfigAws] = None
	_cached_app_config: Optional[ConfigApplication] = None
	_cached_hall_config: Optional[ConfigHall] = None
	_cached_printer_config: Optional[ConfigPrinter] = None

	@classmethod
	def _get_base_path(cls) -> str:
		return os.path.abspath(cls._BASE_PATH)

	@classmethod
	def _get_config_subdir(cls) -> str:
		return os.path.join(cls._get_base_path(), cls._CONFIG_SUBDIR)

	@classmethod
	def _get_data_subdir(cls) -> str:
		return os.path.join(cls._get_base_path(), cls._DATA_SUBDIR)

	@classmethod
	def _get_aws_config_path(cls) -> str:
		return os.path.join(cls._get_config_subdir(), cls._AWS_CONFIG_FILENAME)

	@classmethod
	def _get_app_config_path(cls) -> str:
		return os.path.join(cls._get_config_subdir(), cls._APP_CONFIG_FILENAME)

	@classmethod
	def _get_hall_config_path(cls) -> str:
		return os.path.join(cls._get_config_subdir(), cls._HALL_CONFIG_FILENAME)

	@classmethod
	def _get_printer_config_path(cls) -> str:
		return os.path.join(cls._get_config_subdir(), cls._PRINTER_CONFIG_FILENAME)

	@classmethod
	def get_aws_config(cls) -> ConfigAws:
		if cls._cached_aws_config is None:
			cls._cached_aws_config = ConfigAws.from_filename(cls._get_aws_config_path())
		return cls._cached_aws_config

	@classmethod
	def get_app_config(cls) -> ConfigApplication:
		if cls._cached_app_config is None:
			cls._cached_app_config = ConfigApplication.from_filename(cls._get_app_config_path())
		return cls._cached_app_config

	@classmethod
	def get_hall_config(cls) -> ConfigHall:
		if cls._cached_hall_config is None:
			cls._cached_hall_config = ConfigHall.from_filename(cls._get_hall_config_path())
		return cls._cached_hall_config

	@classmethod
	def get_printer_config(cls) -> ConfigPrinter:
		if cls._cached_printer_config is None:
			cls._cached_printer_config = ConfigPrinter.from_filename(cls._get_printer_config_path())
		return cls._cached_printer_config

	@classmethod
	def get_log_dir_path(cls) -> str:
		return os.path.join(cls._get_base_path(), cls._LOG_SUBDIR)

	@classmethod
	def get_cache_dir_path(cls) -> str:
		return os.path.join(cls._get_base_path(), cls._CACHE_SUBDIR, cls._CACHE_FILENAME)

	@classmethod
	def get_ticket_template_path(cls) -> str:
		return os.path.join(cls._get_data_subdir(), cls._TICKET_TEMPLATE_FILENAME)

	@classmethod
	def get_temp_dir_path(cls) -> str:
		return os.path.join(cls._get_base_path(), cls._TEMP_SUBDIR)

	@classmethod
	def get_capabilities_file_path(cls) -> str:
		return os.path.join(cls._get_data_subdir(), cls._CAPABILITIES_FILENAME)

	@classmethod
	def get_font_path(cls):
		return os.path.join(cls._get_data_subdir(), cls._FONT_FILENAME)
