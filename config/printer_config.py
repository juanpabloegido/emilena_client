from dataclasses import dataclass
from typing import Dict, Any

from parsing import OneWayJsonPropertyType, OneWayJsonPropertyStr, construct_object, OneWayJsonPropertyInt
from .config_tools import IConfig


@dataclass
class ConfigPrinter(IConfig):
	printer_ip: OneWayJsonPropertyType[str] = OneWayJsonPropertyStr(
		'printer_ip', accepts_missing=False, allows_none=False
	)
	printer_port: OneWayJsonPropertyType[int] = OneWayJsonPropertyInt(
		'printer_port', accepts_missing=True, allows_none=True
	)

	@classmethod
	def from_json(cls, parsed_json: Dict[str, Any]) -> 'IConfig':
		return construct_object(cls, parsed_json)
