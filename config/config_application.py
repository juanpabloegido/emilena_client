import sys
from typing import Dict, List, IO, Optional, Any, Callable

from logger_tools import LogType
from .config_tools import IConfig, try_to_parse


class LogTypeConfig:
	log_type: LogType
	filenames: List[str]
	mirrors: List[IO]

	LOG_FILENAME_FIELD: str = 'filenames'
	LOG_MIRROR_FIELD: str = 'mirrors'

	DEF_MIRRORS: Dict[LogType, List[IO]] = {
		LogType.DBG: [sys.stdout],
		LogType.INF: [sys.stdout],
		LogType.WRN: [sys.stderr],
		LogType.ERR: [sys.stderr]
	}
	DEF_FILENAMES: List[str] = ['_combined.log']

	def __init__(self, log_type: LogType, filenames: List[str], mirrors: List[IO]):
		self.log_type = log_type
		self.filenames = filenames
		self.mirrors = mirrors

	@staticmethod
	def _parse_filenames(raw_filenames: List[Any]) -> Optional[List[str]]:
		try:
			return list(map(str, raw_filenames))
		except Exception as e:
			print("Could not parse filenames {}: {!r}".format(raw_filenames, e), file=sys.stderr)
			return None

	@staticmethod
	def _parse_mirrors(raw_mirrors: List[Any]) -> Optional[List[IO]]:
		mapped_files: Dict[str, IO] = {
			'stdout': sys.stdout,
			'stderr': sys.stderr
		}

		try:
			return list(map(lambda raw_mirror: mapped_files[raw_mirror], raw_mirrors))
		except Exception as e:
			print("Could not parse mirrors {}: {!r}".format(raw_mirrors, e), file=sys.stderr)
			return None

	@classmethod
	def build_default_for(cls, log_type: LogType) -> 'LogTypeConfig':
		filenames: List[str] = cls.DEF_FILENAMES
		mirrors: List[IO] = cls.DEF_MIRRORS.get(log_type, [])

		return cls(log_type, filenames, mirrors)

	@classmethod
	def get_defaults(cls) -> Dict[LogType, 'LogTypeConfig']:
		try:
			return {log_type: cls.build_default_for(log_type) for log_type in LogType}
		except Exception as e:
			print("Could not generate default configs for log types: {!r}".format(e))
			return {}

	@classmethod
	def build_from_json(cls, raw_type: str, mapped_config: Dict[str, List[Any]]) -> Optional['LogTypeConfig']:
		try:
			valid_type: LogType = LogType(raw_type)
		except Exception as e:
			print("Could not parse {} as a valid LogType: {!r}".format(raw_type, e), file=sys.stderr)
			return None
		try:
			parsed_filenames: Optional[List[str]] = cls._parse_filenames(mapped_config.get(cls.LOG_FILENAME_FIELD))
			if parsed_filenames is None:
				print("Using default filenames for {}".format(valid_type), file=sys.stderr)
				parsed_filenames = cls.DEF_FILENAMES

			parsed_mirrors: Optional[List[IO]] = cls._parse_mirrors(mapped_config.get(cls.LOG_MIRROR_FIELD))
			if parsed_mirrors is None:
				print("Using default mirrors for {}".format(valid_type), file=sys.stderr)
				parsed_mirrors = cls.DEF_MIRRORS.get(valid_type, [])

			return cls(valid_type, parsed_filenames, parsed_mirrors)
		except Exception as e:
			print("Could not parse {} into log type config: {!r}".format(mapped_config, e), file=sys.stderr)
			return None

	def __repr__(self) -> str:
		return "<{} filenames:{} mirrors:{}>".format(
			type(self),
			self.filenames,
			self.mirrors
		)


class ConfigApplication(IConfig):
	LOG_FIELD: str = 'logger'
	LOG_TYPES_FIELD: str = 'types'

	CLEAN_MIN_FREE_PRC_FIELD: str = 'min_free_disk_cleanup'
	CLEAN_MAX_MONTHS: str = 'max_months_cleanup'

	log_type_config: Dict[LogType, LogTypeConfig]

	clean_min_free_prc: float
	clean_max_months: int

	def __init__(
			self,
			log_type_config: Dict[LogType, LogTypeConfig],
			clean_min_free_prc: float, clean_max_months: int
	):
		self.log_type_config = log_type_config
		self.clean_min_free_prc = clean_min_free_prc
		self.clean_max_months = clean_max_months

	@staticmethod
	def _parse_logger_types(logger_config: Dict[str, Any]) -> Dict[LogType, LogTypeConfig]:
		rv: Dict[LogType, LogTypeConfig] = {}

		for raw_type, raw_config in logger_config.items():
			try:
				valid_type: LogType = LogType(raw_type)

				type_config: LogTypeConfig = LogTypeConfig.build_from_json(raw_type, raw_config)
				if type_config is None:
					print("Could not parse configuration for {}, using defaults", file=sys.stderr)
					type_config: LogTypeConfig = LogTypeConfig.build_default_for(valid_type)

				rv[valid_type] = type_config
			except Exception as e:
				print("Could not parse {} as log type: {!r}".format(raw_type, e), file=sys.stderr)

		for expected_type in LogType:
			parsed_config: Optional[LogTypeConfig] = rv.get(expected_type)
			if parsed_config is None:
				print("No config found for {}, using defaults".format(expected_type), file=sys.stderr)
				rv[expected_type] = LogTypeConfig.build_default_for(expected_type)

		return rv

	@staticmethod
	def _parse_int(raw_int: str) -> Optional[int]:
		try:
			return int(raw_int)
		except Exception as e:
			print("Could not parse {} as int: {!r}".format(raw_int, e), file=sys.stderr)
			return None

	@staticmethod
	def _parse_float(raw_float) -> Optional[float]:
		try:
			return int(raw_float)
		except Exception as e:
			print("Could not parse {} as float: {!r}".format(raw_float, e), file=sys.stderr)
			return None

	@classmethod
	def from_json(cls, parsed_json: Dict[str, Any]) -> 'ConfigApplication':
		logger_config: Dict[str, Any] = parsed_json.get(cls.LOG_FIELD)

		log_type_config: Dict[LogType, LogTypeConfig] = \
			try_to_parse(logger_config, cls.LOG_TYPES_FIELD, cls._parse_logger_types, LogTypeConfig.get_defaults)

		clean_min_free_prc: Optional[float] = \
			try_to_parse(parsed_json, cls.CLEAN_MIN_FREE_PRC_FIELD, cls._parse_float)
		clean_max_months: Optional[int] = \
			try_to_parse(parsed_json, cls.CLEAN_MAX_MONTHS, cls._parse_int)

		return cls(
			log_type_config,
			clean_min_free_prc,
			clean_max_months
		)

	def __repr__(self) -> str:
		return (
			"<{} log_type_config:{}"
			" clean_min_free_prc:{} clean_max_months:{} >".format(
				type(self),
				self.log_type_config,
				self.clean_min_free_prc,
				self.clean_max_months
			))
