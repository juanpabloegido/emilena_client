from typing import Optional
import boto3
import datetime
import json


"""All datetimes MUST be on UTC, since no timezone info is stored"""
DATE_FORMAT: str = '%Y-%m-%d'
TIME_FORMAT: str = '%H:%M:%S'
DATETIME_FORMAT: str = '{} {}'.format(DATE_FORMAT, TIME_FORMAT)


def str_to_datetime(str_dt: Optional[str]) -> Optional[datetime.datetime]:
    """Datetimes obtained through this method will have their timezones set to UTC"""
    if str_dt is None:
        return None

    dt: datetime.datetime = datetime.datetime.strptime(str_dt, DATETIME_FORMAT)
    return _to_utc(dt)


def date_to_str(d: Optional[datetime.date]) -> Optional[str]:
    if d is None:
        return None

    return d.strftime(DATETIME_FORMAT)


def str_to_date(str_d: Optional[str]) -> Optional[datetime.date]:
    if str_d is None:
        return None

    return datetime.datetime.strptime(str_d, DATETIME_FORMAT).date()


def _to_utc(dt: Optional[datetime.datetime]) -> Optional[datetime.datetime]:
    if dt is None:
        return None

    if dt.tzinfo is None:
        return dt.replace(tzinfo=datetime.timezone.utc)

    return dt.astimezone(datetime.timezone.utc)


def utc_now() -> datetime.datetime:
    dt: datetime.datetime = datetime.datetime.utcnow()
    return _to_utc(dt)


def invoke_lambda_function(function_name: str = None, payload=None):
    if function_name is None:
        raise Exception('ERROR: functionName parameter cannot be NULL')
    payload_str = json.dumps(payload)
    payload_bytes_arr = bytes(payload_str, encoding='utf8')
    client = boto3.client('lambda')
    rv = client.invoke(
        FunctionName=function_name,
        InvocationType="RequestResponse",
        Payload=payload_bytes_arr
    )
    return rv
