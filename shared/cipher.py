from base64 import b64decode, b64encode
from typing import Union, Optional

from Cryptodome.Cipher import AES

from secret_reader import SecretReader


class AesCipher:
	_AES_KEY_FIELD: str = 'AES_KEY'

	_key_128: bytes

	@staticmethod
	def _fold_key(key_256: bytes) -> bytes:
		final_key: bytearray = bytearray(16)
		for i, c in enumerate(key_256):
			final_key[i % 16] ^= key_256[i]
		return bytes(final_key)

	def __init__(self, key: bytes):
		"""
		Key can be either 128 or 256 bits, but it will always get folded to 128 bits, since that's all MySql supports
		https://stackoverflow.com/questions/51134744/how-to-use-implement-aes-decrypt-of-mysql-by-python
		"""
		if len(key) == 32:  # 256 bits key, fold it
			self._key_128 = type(self)._fold_key(key)
		elif len(key) == 16:  # 128 bits key, keep it as is
			self._key_128 = key
		else:
			raise ValueError("Provided key has the wrong length in bytes ({}), needs to be either 256 or 128".format(
				len(key)
			))

	@classmethod
	def from_b64_key(cls, b64_key: str) -> 'AesCipher':
		key_bytes: bytes = b64decode(b64_key)
		return cls(key_bytes)

	@classmethod
	def from_secret(cls) -> 'AesCipher':
		b64_key: str = SecretReader.read_secret(cls._AES_KEY_FIELD)
		return cls.from_b64_key(b64_key)

	def encrypt(self, unencrypted_bytes: bytes) -> bytes:
		padded_bytes = type(self)._pad_bytes(unencrypted_bytes)

		# Type of instance depends on mode. Sadly, no common base for implementations
		aes_instance = AES.new(self._key_128, AES.MODE_ECB)

		return aes_instance.encrypt(padded_bytes)

	def encrypt_to_b64(self, unencrypted_bytes: bytes) -> str:
		encrypted_bytes: bytes = self.encrypt(unencrypted_bytes)
		b64_bytes: bytes = b64encode(encrypted_bytes)
		return b64_bytes.decode()

	def decrypt(self, encrypted_bytes: Union[bytes, str]) -> bytes:
		aes_instance = AES.new(self._key_128, AES.MODE_ECB)

		padded_bytes: bytes = aes_instance.decrypt(encrypted_bytes)

		return type(self)._unpad_bytes(padded_bytes)

	def decrypt_from_b64(self, encrypted_b64) -> bytes:
		encrypted_bytes: bytes = b64decode(encrypted_b64)
		return self.decrypt(encrypted_bytes)

	def decrypt_from_b64_decode(self, encrypted_b64) -> Optional[str]:
		if encrypted_b64 is None:
			return None
		encrypted_bytes: bytes = b64decode(encrypted_b64)
		return self.decrypt(encrypted_bytes).decode('UTF-8')

	@staticmethod
	def _unpad_bytes(raw_bytes: bytes) -> bytes:
		return raw_bytes[:-raw_bytes[-1]]

	@staticmethod
	def _pad_bytes(raw_bytes: bytes) -> bytes:
		padding_needed: int = (AES.block_size - (len(raw_bytes) % AES.block_size))
		if padding_needed == 0:  # Padding is always present, even when it would not be necessary to form blocks
			padding_needed = AES.block_size

		return raw_bytes + padding_needed * bytes([padding_needed])
