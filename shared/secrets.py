import base64
import hashlib
import hmac

import passlib.handlers.sha2_crypt
from passlib.context import CryptContext

"""This line is necessary for pyinstaller to detect the hidden import"""
ALG = passlib.handlers.sha2_crypt

from secret_reader import SecretReader


class SecretsManager:
	_PASSWORD_HMAC_KEY_FIELD: str = 'HMAC_KEY'
	_SECURITY_HASH_ALG = 'sha512_crypt'
	_STRING_ENCODING: str = 'utf-8'

	@classmethod
	def _get_key(cls) -> bytes:
		raw_secret: str = SecretReader.read_secret(cls._PASSWORD_HMAC_KEY_FIELD)
		return cls._encode_str(raw_secret)

	@classmethod
	def _encode_str(cls, s: str) -> bytes:
		return s.encode(cls._STRING_ENCODING)

	@classmethod
	def verify_password(cls, password: str, expected_hash: str) -> bool:
		key: bytes = cls._get_key()
		encoded_password: bytes = cls._encode_str(password)
		h: hmac.HMAC = hmac.new(key=key, msg=encoded_password, digestmod=hashlib.sha512)
		hashed_password: str = base64.b64encode(h.digest())
		encoded_expected_hash: bytes = cls._encode_str(expected_hash)

		crypt_context: CryptContext = CryptContext(schemes=[cls._SECURITY_HASH_ALG])
		return crypt_context.verify(secret=hashed_password, hash=encoded_expected_hash)
