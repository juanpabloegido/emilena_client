from dataclasses import dataclass
from typing import Type, Optional

from db.model import Prohibition
from logic import AesCipher
from .base import DecryptedModel


@dataclass
class DecryptedProhibition(DecryptedModel[Prohibition]):
	doc_type: str
	doc: str

	first_name: str
	last_name_first: str
	last_name_second: str
	local_type: str

	@classmethod
	def _get_model_type(cls) -> Type[Prohibition]:
		return Prohibition

	def encrypt_into(self, cipher: AesCipher, model: Prohibition) -> None:
		def encrypt(unencrypted: Optional[str]) -> Optional[str]:
			return type(self)._encrypt_to_b64(unencrypted, cipher)

		model.doc_type = self.doc_type
		model.doc = encrypt(self.doc)

		model.first_name = encrypt(self.first_name)
		model.last_name_first = encrypt(self.last_name_first)
		model.last_name_second = encrypt(self.last_name_second)
		model.local_type = self.local_type

	@classmethod
	def decrypt_from(cls, cipher: AesCipher, model: Prohibition) -> 'DecryptedProhibition':
		def decrypt(encrypted: Optional[str]) -> Optional[str]:
			return cls._decrypt_from_b64(encrypted, cipher)

		return cls(
			doc_type=model.doc_type,
			doc=decrypt(model.doc),

			first_name=decrypt(model.first_name),
			last_name_first=decrypt(model.last_name_first),
			last_name_second=decrypt(model.last_name_second),
			local_type=model.local_type
		)
