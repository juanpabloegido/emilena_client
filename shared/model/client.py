import datetime
from dataclasses import dataclass
from typing import Optional, Type, Any

from db.model import Client
from .base import DecryptedModel
from ..cipher import AesCipher


@dataclass
class DecryptedClient(DecryptedModel[Client]):
	nif: str
	doc_is_not_nif: bool
	name: str
	alias: Optional[str]
	doc_expiration_date: datetime.date

	gender: str
	birth_date: datetime.date

	email: Optional[str]
	phone: Optional[str]

	country: str
	province: Optional[str]
	locality: Optional[str]
	postal_code: Optional[str]
	address: Optional[str]

	is_access_denied: bool
	is_problematic: bool
	is_vip: bool
	is_professional_player: bool

	observations: Optional[str]
	incidence_report: Optional[str]

	@classmethod
	def _get_model_type(cls) -> Type[Client]:
		return Client

	def encrypt_into(self, cipher: AesCipher, model: Client) -> None:
		def encrypt(unencrypted: Optional[str]) -> Optional[str]:
			return type(self)._encrypt_to_b64(unencrypted, cipher)

		model.nif = encrypt(self.nif)
		model.doc_is_not_nif = self.doc_is_not_nif
		model.name = encrypt(self.name)
		model.alias = encrypt(self.alias)
		model.doc_expiration_date = encrypt(type(self).date_to_str(self.doc_expiration_date))

		model.gender = encrypt(self.gender)
		model.birth_date = encrypt(type(self).date_to_str(self.birth_date))

		model.email = encrypt(self.email)
		model.phone = encrypt(self.phone)

		model.country = encrypt(self.country)
		model.province = encrypt(self.province)
		model.locality = encrypt(self.locality)
		model.postal_code = encrypt(self.postal_code)
		model.address = encrypt(self.address)

		model.is_access_denied = self.is_access_denied
		model.is_problematic = self.is_problematic
		model.is_vip = self.is_vip
		model.is_professional_player = self.is_professional_player

		model.observations = self.observations,
		model.incidence_report = self.incidence_report

	@classmethod
	def decrypt_from(cls, cipher: AesCipher, model: Client) -> 'DecryptedClient':
		def decrypt(encrypted: Optional[str]) -> Optional[str]:
			return cls._decrypt_from_b64(encrypted, cipher)

		return cls(
			nif=decrypt(model.nif),
			doc_is_not_nif=model.doc_is_not_nif,
			name=decrypt(model.name),
			alias=decrypt(model.alias),
			doc_expiration_date=cls.str_to_date(decrypt(model.doc_expiration_date)),

			gender=decrypt(model.gender),
			birth_date=cls.str_to_date(decrypt(model.birth_date)),

			email=decrypt(model.email),
			phone=decrypt(model.phone),

			country=decrypt(model.country),
			province=decrypt(model.province),
			locality=decrypt(model.locality),
			postal_code=decrypt(model.postal_code),
			address=decrypt(model.address),

			is_access_denied=model.is_access_denied,
			is_problematic=model.is_problematic,
			is_vip=model.is_vip,
			is_professional_player=model.is_professional_player,

			observations=model.observations,
			incidence_report=model.incidence_report
		)

	def get_order_key(self) -> Any:
		return self.name
