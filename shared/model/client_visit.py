import datetime
from dataclasses import dataclass
from enum import Enum
from typing import Optional, Type

from db.model import ClientHallVisit
from gui.shared import TimestampFormatter
from .base import DecryptedModel
from ..cipher import AesCipher


class EntranceStatus(Enum):
	ENTRANCE_ALLOWED: str = "ALLOWED"
	ENTRANCE_DENIED_MINOR: str = "MINOR"
	ENTRANCE_DENIED_DOC_EXPIRED: str = "DOC_EXPIRED"
	ENTRANCE_DENIED_DOC_PROHIBITED: str = "DOC_PROHIBITED"
	ENTRANCE_DENIED_ACCESS_REVOKED: str = "ACCESS_REVOKED"


def _safe_export_entrance_status(entrance_status: Optional[EntranceStatus]) -> Optional[EntranceStatus]:
	if entrance_status is None:
		return None
	return entrance_status.value


def _safe_import_entrance_status(raw_entrance_status: Optional[str]) -> Optional[EntranceStatus]:
	if raw_entrance_status is None:
		return None
	try:
		return EntranceStatus(raw_entrance_status)
	except ValueError:
		return None


@dataclass
class DecryptedClientHallVisit(DecryptedModel[ClientHallVisit]):
	user_id: int
	hall_code: str

	entrance_status: EntranceStatus

	client_id: Optional[int]
	entered_in_group: Optional[bool]

	name: str
	alias: Optional[str]
	nif: str
	doc_is_not_nif: bool
	doc_expiration_date: datetime.date
	gender: str
	birth_date: datetime.date
	country: str
	legal_hall_id: str

	observations: Optional[str]

	time_in: datetime.datetime
	time_out: Optional[datetime.datetime]

	@classmethod
	def _get_model_type(cls) -> Type[ClientHallVisit]:
		return ClientHallVisit

	def encrypt_into(self, cipher: AesCipher, model: ClientHallVisit) -> None:
		def encrypt(unencrypted: Optional[str]) -> Optional[str]:
			return type(self)._encrypt_to_b64(unencrypted, cipher)

		model.user_id = self.user_id
		model.hall_code = self.hall_code

		model.entrance_status = _safe_export_entrance_status(self.entrance_status)

		model.client_id = self.client_id
		model.entered_in_group = self.entered_in_group

		model.name = encrypt(self.name)
		model.alias = encrypt(self.alias)
		model.nif = encrypt(self.nif)
		model.doc_is_not_nif = self.doc_is_not_nif
		model.doc_expiration_date = encrypt(type(self).date_to_str(self.doc_expiration_date))
		model.gender = encrypt(self.gender)
		model.birth_date = encrypt(type(self).date_to_str(self.birth_date))
		model.country = encrypt(self.country)
		model.legal_hall_id = self.legal_hall_id

		model.observations = self.observations

		model.time_in = type(self)._to_utc(self.time_in)
		model.time_out = type(self)._to_utc(self.time_out)

	@classmethod
	def decrypt_from(cls, cipher: AesCipher, model: ClientHallVisit) -> 'DecryptedClientHallVisit':
		def decrypt(encrypted: Optional[str]) -> Optional[str]:
			return cls._decrypt_from_b64(encrypted, cipher)

		return cls(
			user_id=model.user_id,
			hall_code=model.hall_code,

			entrance_status=_safe_import_entrance_status(model.entrance_status),

			client_id=model.client_id,
			entered_in_group=model.entered_in_group,

			name=decrypt(model.name),
			alias=decrypt(model.alias),
			nif=decrypt(model.nif),
			doc_is_not_nif=model.doc_is_not_nif,
			doc_expiration_date=cls.str_to_date(decrypt(model.doc_expiration_date)),
			gender=decrypt(model.gender),
			birth_date=cls.str_to_date(decrypt(model.birth_date)),
			country=decrypt(model.country),
			legal_hall_id=model.legal_hall_id,

			observations=model.observations,

			time_in=cls._to_utc(model.time_in),
			time_out=cls._to_utc(model.time_out)
		)

	def formatted_time_in(self) -> Optional[str]:
		if self.time_in is None:
			return None

		local_dt: datetime.datetime = TimestampFormatter.to_local_datetime(self.time_in)
		return TimestampFormatter.datetime_to_str(local_dt)
