from dataclasses import dataclass
from typing import Optional, Type

from db.model import User
from .base import DecryptedModel
from ..cipher import AesCipher


@dataclass
class DecryptedUser(DecryptedModel[User]):
	name: Optional[str]
	email: Optional[str]
	nif: Optional[str]

	username: str
	password: str

	@classmethod
	def _get_model_type(cls) -> Type[User]:
		pass

	def encrypt_into(self, cipher: AesCipher, model: User) -> None:
		model.name = self.name
		model.email = self.email
		model.nif = self.nif

		model.username = self.username
		model.password = self.username

	@classmethod
	def decrypt_from(cls, cipher: AesCipher, model: User) -> 'DecryptedUser':
		return cls(
			name=model.name,
			email=model.email,
			nif=model.nif,

			username=model.username,
			password=model.password
		)
