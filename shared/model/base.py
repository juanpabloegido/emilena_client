import datetime
from abc import ABC, abstractmethod
from base64 import b64encode, b64decode
from typing import TypeVar, Generic, Optional, Type

from ..cipher import AesCipher

EncryptedDbModelType: TypeVar = TypeVar('EncryptedDbModelType')


class DecryptedModel(Generic[EncryptedDbModelType], ABC):
	"""All datetimes MUST be on UTC, since no timezone info is stored"""
	DATE_FORMAT: str = '%Y-%m-%d'
	TIME_FORMAT: str = '%H:%M:%S'
	DATETIME_FORMAT: str = '{} {}'.format(DATE_FORMAT, TIME_FORMAT)

	@classmethod
	@abstractmethod
	def _get_model_type(cls) -> Type[EncryptedDbModelType]:
		pass

	@abstractmethod
	def encrypt_into(self, cipher: AesCipher, model: EncryptedDbModelType) -> None:
		pass

	def encrypt_into_new(self, cipher: AesCipher) -> EncryptedDbModelType:
		model_type: Type[EncryptedDbModelType] = type(self)._get_model_type()
		new_model_instance: EncryptedDbModelType = model_type()

		self.encrypt_into(cipher, new_model_instance)
		return new_model_instance

	@classmethod
	@abstractmethod
	def decrypt_from(cls, cipher: AesCipher, model: EncryptedDbModelType) -> 'DecryptedModel':
		pass

	@classmethod
	def create_from_empty(cls, cipher: AesCipher) -> 'DecryptedModel':
		model_type: Type[EncryptedDbModelType] = cls._get_model_type()

		return cls.decrypt_from(cipher, model_type())

	@classmethod
	def datetime_to_str(cls, dt: Optional[datetime.datetime]) -> Optional[str]:
		"""If the datetime has a valid timezone, it will be translated to UTC first"""
		if dt is None:
			return None

		dt = cls._to_utc(dt)

		return dt.strftime(cls.DATETIME_FORMAT)

	@classmethod
	def str_to_datetime(cls, str_dt: Optional[str]) -> Optional[datetime.datetime]:
		"""Datetimes obtained through this method will have their timezones set to UTC"""
		if str_dt is None:
			return None

		dt: datetime.datetime = datetime.datetime.strptime(str_dt, cls.DATETIME_FORMAT)
		return cls._to_utc(dt)

	@classmethod
	def date_to_str(cls, d: Optional[datetime.date]) -> Optional[str]:
		if d is None:
			return None

		return d.strftime(cls.DATE_FORMAT)

	@classmethod
	def str_to_date(cls, str_d: Optional[str]) -> Optional[datetime.date]:
		if str_d is None:
			return None

		return datetime.datetime.strptime(str_d, cls.DATE_FORMAT).date()

	@staticmethod
	def _encrypt_to_b64(unencrypted: Optional[str], cipher: AesCipher) -> Optional[str]:
		if unencrypted is None:
			return None
		return b64encode(cipher.encrypt(unencrypted.encode())).decode()

	@staticmethod
	def _decrypt_from_b64(encrypted_b64: Optional[str], cipher: AesCipher) -> Optional[str]:
		if encrypted_b64 is None:
			return None
		return cipher.decrypt(b64decode(encrypted_b64)).decode()

	@classmethod
	def _to_utc(cls, dt: Optional[datetime.datetime]) -> Optional[datetime.datetime]:
		if dt is None:
			return None

		if dt.tzinfo is None:
			return dt.replace(tzinfo=datetime.timezone.utc)

		return dt.astimezone(datetime.timezone.utc)

	@classmethod
	def utc_now(cls) -> datetime.datetime:
		dt: datetime.datetime = datetime.datetime.utcnow()
		return cls._to_utc(dt)
