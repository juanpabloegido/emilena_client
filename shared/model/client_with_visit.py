import datetime
from dataclasses import dataclass
from typing import Optional

from db.model import ClientWithVisit
from logic import AesCipher
from .client import DecryptedClient
from .client_visit import DecryptedClientHallVisit


@dataclass
class DecryptedClientWithVisit:
	client: DecryptedClient
	visit: DecryptedClientHallVisit

	@classmethod
	def decrypt_from(cls, aes_cipher: AesCipher, client_with_visit: ClientWithVisit) -> 'DecryptedClientWithVisit':
		return cls(
			DecryptedClient.decrypt_from(aes_cipher, client_with_visit.client),
			DecryptedClientHallVisit.decrypt_from(aes_cipher, client_with_visit.visit)
		)

	def get_nif(self) -> Optional[str]:
		return self.visit.nif

	def get_is_passport(self) -> Optional[bool]:
		return self.visit.doc_is_not_nif

	def get_name(self) -> Optional[str]:
		return self.visit.name

	def get_alias(self) -> Optional[str]:
		return self.visit.alias if self.visit.alias is not None else self.client.alias

	def get_birth_date(self) -> Optional[datetime.date]:
		return self.visit.birth_date
